package streamproblems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringNStreamProblems
{
    public static void main(String[] args)
    {


        Stream<String> strStream = Stream.of("abc", "ii", "gjuyyy", "vvvv");

        List<String> strList = strStream.filter( s -> s.length() > 2 ).collect(Collectors.toList());
        System.out.println(strList);

        Stream<String> strStream2 = Stream.of( "india", "abc"," ii", "gjuyyy", "vvvv", "usa", "japan");
        List<String> strList2 = strStream2.map(s -> s.toUpperCase()).collect(Collectors.toList());
        System.out.println(strList2);

        Stream<Integer> intStream = Stream.of(2, 5, 7, 5, 4, 2, 3, 8);
        List<Integer> intList =  intStream.distinct().map(i -> i * 2).collect(Collectors.toList());
        System.out.println(intList);

        Stream<Integer> intStream2 = Stream.of(2, 5, 7, 5, 4, 2, 3, 8);
        long count = intStream2.filter( i -> i == 2).count();
        System.out.println("No of times 2 occurred: " + count);


        String[][] data = new String[][]{{"a", "b"}, {"c", "d"}, {"e", "f"}};
        Stream<String[]> temp = Arrays.stream(data);
       // System.out.println(temp.filter(s -> s.equals("a")).count());
        Stream<String> sts = temp.flatMap(x -> Arrays.stream(x));
        long acount = sts.filter(arElement -> arElement.equals("a")).count();
        System.out.println("a found : " + acount);
           //     (;

    }
}
