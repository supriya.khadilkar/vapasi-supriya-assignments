package problems;

/**
 * Java Basic Hands-on: #5
 */
public class FactorialCalculator
{
    public static int calculateFactorial(int input)
    {
        int fact = 1;
        for(int i = 1; i <= input; i++)
        {
            fact = fact * i;
        }
        return fact;
    }

    public static void main(String[] args) {
        int input = 5;
        System.out.println(calculateFactorial(input));

        input = 7;
        System.out.println(calculateFactorial(input));
    }
}
