package problems;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Collections Hands-on to try ArrayList
 */
public class ArrayListDemo
{
    public static void main(String[] args)
    {
        ArrayList<Integer> nums = new ArrayList<>();
        System.out.println(nums.size());
        nums.add(4);
        nums.add(41);
        nums.add(44);
        nums.add(56);

        int sum = 0 ;

        nums.remove(2);
        Iterator it = nums.iterator();
        while (it.hasNext())
        {
            sum = sum + (Integer) it.next();
        }
        System.out.println("Sum of all element is: " + sum);

        /*for(Integer num: nums) {
            System.out.println(num);
        }*/
        nums.forEach(node -> {
            if(node % 2 == 0)
                System.out.println(node);
        });

    }
}
