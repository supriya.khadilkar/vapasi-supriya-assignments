package problems;

import java.util.*;

public class HashMapDemo
{
    static HashMap<String, HashSet<String>> inventory = new HashMap<>();
    public static void main(String[] args)
    {
        HashMap<Integer, String> rolls = new HashMap<>();
        rolls.put(1,"ABC");rolls.put(2,"jjj");
        rolls.put(10,"TTT");rolls.put(3,"lll");
        rolls.put(9,"zzz");rolls.put(4,"qqq");
        rolls.put(8,"ooo");rolls.put(5,"iiii");
        rolls.put(7,"www");rolls.put(6,"ppp");

        HashMap<Integer, Integer> rollsWiseMarks = new HashMap<>();
        rollsWiseMarks.put(1,1);rollsWiseMarks.put(2,5);
        rollsWiseMarks.put(10,2);rollsWiseMarks.put(3,2);
        rollsWiseMarks.put(9,3);rollsWiseMarks.put(4,1);
        rollsWiseMarks.put(8,4);rollsWiseMarks.put(5,9);
        rollsWiseMarks.put(7,5);rollsWiseMarks.put(6,7);

        for(Map.Entry e : rolls.entrySet())
        {
            System.out.println("Roll Id: " + e.getKey() + "\tName: " + e.getValue() + "\t Marks: "+rollsWiseMarks.get(e.getKey()));
        }

        TreeMap<String, Set<String>> dcts = new TreeMap<String, Set<String>>();
        HashSet<String> set = new HashSet<>();
        set.add("Aere");
        set.add("abaa");
        set.add("atyh");
        set.add("abdd");
        dcts.put("A", set);
        HashSet<String> set1 = new HashSet<>();
        set1.add("bhy");
        set1.add("bli");
        set1.add("bbb");
        set1.add("bat");
        dcts.put("B", set1);

        for(Map.Entry e: dcts.entrySet())
        {
            System.out.println(e.getKey() + "\t" + e.getValue());
        }

    }

    public static void addBrandToInventory(String brandName, HashSet<String> accessories)
    {
        inventory.put(brandName, accessories);
    }

    public static void removeBrandFromInventory(String brandName)
    {
        if(inventory.containsKey(brandName))
            inventory.remove(brandName);
    }

    public static void addAccessoryToBrand(String brand, String accessory)
    {
        if(inventory.containsKey(brand))
        {
            inventory.get(brand).add(accessory);
        }
    }

    public static void addAccessoryToBrand(String brand, HashSet<String> accessories)
    {
        if(inventory.containsKey(brand))
        {
            inventory.get(brand).addAll(accessories);
        }
        else {
            HashSet<String> set = new HashSet<>();
            set.addAll(accessories);
            inventory.put(brand, set);
        }
    }

    public static void deleteAccessoryFromBrand(String brand, String accessory)
    {
        if(inventory.containsKey(brand))
        {
            HashSet<String> set = inventory.get(brand);
            if(!set.isEmpty())
            {
                if(set.contains(accessory))
                    set.remove(accessory);
            }
        }
    }

    //inventory if levis
    //levis -> jeans


    public static void deleteAccessoryFromBrand(String brand, HashSet<String> accessories)
    {
        if(inventory.containsKey(brand))
        {
            HashSet<String> set = inventory.get(brand);
            if(!set.isEmpty())
            {
                if(set.containsAll(accessories))
                    set.removeAll(accessories);
            }
        }
    }

}
