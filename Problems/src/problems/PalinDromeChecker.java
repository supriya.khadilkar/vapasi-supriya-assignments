package problems;

public class PalinDromeChecker
{
    public  static  void main(String[] args)
    {
        String input = "mamdam";
        boolean isPalindrome = true;
        int start = 0; int end = input.length() - 1;
        int mid = (start + end) / 2;
        for(;start<mid && end>mid; start++, end--)
        {
            if(input.charAt(start) != input.charAt(end)) {
                isPalindrome = false;
                break;
            }
        }
        if(isPalindrome)
            System.out.println("Given input is a palindrome");
        else
            System.out.println("Given input is not a palindrome");
    }
}
