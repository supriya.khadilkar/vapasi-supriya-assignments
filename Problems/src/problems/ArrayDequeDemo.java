package problems;

import java.util.ArrayDeque;

/**
 * Array-Deque hands-on
 */
public class ArrayDequeDemo
{
    public static void main(String[] args)
    {
        ArrayDeque<String> dqs = new ArrayDeque<>();
        dqs.add("A");
        dqs.add("B");
        dqs.add("C");
        dqs.add("D");
        dqs.add("E");
        System.out.println(dqs.peek());
       // System.out.println(dqs.remove());
        System.out.println(dqs.remove("C"));
        System.out.println(dqs.contains("C"));

    }



}
