package problems;

public class Fibonacci
{
    /**
     *
     * Java Basics hands-on #1
     * Fibonacci
     */
    public static void main(String[] args)
    {
        int num1 = 0, num2 = 1, input = 10, sum = num1 + num2;
        int i = 2;
        while(i < input)
        {
            //System.out.println(num1);
            //System.out.println(num2);
            System.out.println(sum);
            num1 = num2; num2 = sum;
            sum = num1 + num2;
            i++;
        }
    }
}
