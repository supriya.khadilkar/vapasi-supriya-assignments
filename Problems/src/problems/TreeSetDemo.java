package problems;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class TreeSetDemo
{
    public static void main(String[] args) {
        TreeSet<Integer> numSet = new TreeSet();
        numSet.add(5);
        numSet.add(1);
        numSet.add(8);
        numSet.add(65);
        numSet.add(45);
        numSet.add(40);
        numSet.add(6);

        //numSet.forEach(node -> System.out.println(node));
        Iterator it = numSet.iterator();
        while(it.hasNext())
        {
            System.out.println(it.next());
        }

        LinkedHashSet<String> strsHashSet = new LinkedHashSet<>();
        strsHashSet.add("Hello");
        strsHashSet.add("World");
        strsHashSet.add("hash sets");
        strsHashSet.add("world");

        strsHashSet.remove("hash sets");
        Iterator its = strsHashSet.iterator();
        while (its.hasNext()) {
            System.out.println(its.next());
        }

        HashSet<Integer> numSet1 = new HashSet<>();
        HashSet<Integer> numSet2 = new HashSet<>();

        numSet1.add(1);
        numSet1.add(2);
        numSet1.add(3);        numSet1.add(5);

        numSet2.add(3);
        numSet2.add(4);
        numSet2.add(5);
        numSet2.add(9);
        numSet2.add(10);
    }

    public static HashSet<Integer> intersectionSet(HashSet<Integer> set1, HashSet<Integer> set2)
    {
        return null;
    }
}
