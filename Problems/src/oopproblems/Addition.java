package oopproblems;

public interface Addition extends Operation
{
     int add(int number1, int number2);
     float add(float number1, float number2);
     double add(double number1, double number2);
     long add(long number1, long number2);
}
