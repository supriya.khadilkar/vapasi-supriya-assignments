package shoppingproblemv1;

public class Item
{
    private String itemName;
    private Integer itemId;
    private Brand brand;
    private ItemType itemType;


    public Item()
    {

    }
    public Item(String itemName, Integer itemId, Brand brand, ItemType itemType)
    {
        this.itemName = itemName;
        this.itemId = itemId;
        this.brand = brand;
        this.itemType = itemType;
    }

    public String getItemName() {
        return itemName;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
    public Brand getBrand() {
        return brand;
    }
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

}
