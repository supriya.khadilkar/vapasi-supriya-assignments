package shoppingproblemv1;

public enum ItemType
{
    Accessory,
    Clothing,
    Luggage,
    Perfume
}
