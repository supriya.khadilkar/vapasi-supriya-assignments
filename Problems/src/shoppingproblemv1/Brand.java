package shoppingproblemv1;

import java.io.Serializable;

public class Brand implements Serializable
{
    private String brandName;

    public String getBrandName() {
        return brandName;
    }
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Brand()
    {

    }

    public Brand(String brandName)
    {
        this.brandName = brandName;
    }
}
