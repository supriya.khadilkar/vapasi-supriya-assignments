package shoppingproblemv1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * extension of shopping
 */
public class ShoppingApp1
{
    static volatile List<Item> items = new ArrayList<>();
    static volatile List<Brand> brands = new ArrayList<>();
    static volatile List<ItemType> itemTypeList = new ArrayList<>();
    public static void main(String[] args)
    {



//        System.out.println("Items found: \n" + searchByAccessory(acc));
//        System.out.println("Item Found by Brand: \n" + searchByBrand("PARKER"));
//        System.out.println("Item found by Item Name: \n" + searchByItemName("Floaters"));

       // searchByAccessory(acc).stream().forEach(i -> System.out.println(i.getItemName() + " " + i.getBrand().getBrandName()));

    }

    public void addBrand(final String bName)
    {
        Brand br = new Brand(bName);
        brands.add(br);
    }

    public static List<Item> searchByAccessory(final ItemType itemType)
    {
        List<Item> list = new ArrayList<>();
        list = items.stream().filter(it -> it.getItemType().equals(itemType)).collect(Collectors.toList());
        //long count = items.stream().filter(it -> it.getItemType().equals(itemType)).count();
        if(null == list || list.isEmpty())
            list = new ArrayList<>();
        return list;
    }


    public static List<Item> searchByBrand(final String brandName)
    {
        List<Item> list = new ArrayList<>();
        list = items.stream().filter(it -> it.getBrand().getBrandName().equals(brandName)).collect(Collectors.toList());
        if(null == list || list.isEmpty())
            list = new ArrayList<>();
        return list;
    }

    public static List<Item> searchByItemName(final String itemName)
    {
        List<Item> list = new ArrayList<>();
        list = items.stream().filter(it -> it.getItemName().equals(itemName)).collect(Collectors.toList());
        if(null == list || list.isEmpty())
            list = new ArrayList<>();
        return list;
    }

    public static void addCart(Item item)
    {

    }

    public static void addCart(List<Item> items)
    {

    }

}
