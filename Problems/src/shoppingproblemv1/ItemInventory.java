package shoppingproblemv1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public  class ItemInventory
{
    private static volatile ItemInventory inventory;


    private ItemInventory() { }

    public static ItemInventory getItemInventory()
    {
        if(null == inventory) {
            //fillInventory();
            inventory = new ItemInventory();
        }
        return inventory;
    }

}
