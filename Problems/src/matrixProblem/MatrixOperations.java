package matrixProblem;

import java.util.Random;

/**
 *  Java Basics Problem
 * @author: Supriya
 * @Description
 *  Putting random integers to 2-D array, then printing them
 *  Adding two matrices
 *  Multiplying two matrices
 *
 */
public class MatrixOperations
{
    public static  void main(String[] args)
    {
        //int[][] matrix1 = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}, {13,14,15,16}, {17,18,19,20}};
        int row, rowIndex, col, colIndex;

        /*Matrix Addition*/
        System.out.println("******* Adding two matrices ***************");
        row =  col = 3;
        int[][] matrix2 = new int[row][col];
        Random random = new Random(5);
        for(rowIndex = 0; rowIndex < row; rowIndex++)
        {
            for (colIndex = 0; colIndex < col; colIndex++)
            {
                matrix2[rowIndex][colIndex] = random.nextInt(70);
            }
        }

        printMatrix(matrix2, row, col,1);

        int[][] matrix3 = new int[row][col];
        for(rowIndex = 0; rowIndex < row; rowIndex++)
        {
            for (colIndex = 0; colIndex < col; colIndex++)
            {
                matrix3[rowIndex][colIndex] = random.nextInt(70);
            }
        }

        printMatrix(matrix3, row, col,2);

        addMatrix(matrix2, matrix3, row, col);
        System.out.println("***************************************************");
        System.out.println("********** Multiplying two matrices ***************");
       int[][] matrix4 = new int[row][col];
        for(rowIndex = 0; rowIndex < row; rowIndex++)
        {
            for (colIndex = 0; colIndex < col; colIndex++)
            {
                matrix4[rowIndex][colIndex] = random.nextInt(70);
            }
        }
        printMatrix(matrix4, row, col,3);

        int[][] matrix5 = new int[row][col];
        for(rowIndex = 0; rowIndex < row; rowIndex++)
        {
            for (colIndex = 0; colIndex < col; colIndex++)
            {
                matrix5[rowIndex][colIndex] = random.nextInt(70);
            }
        }
        printMatrix(matrix5, row, col,4);
        multiplyMatrix(matrix4, matrix5, row, col);
        System.out.println("**************************************************");


    }

    public static void printMatrix(int[][] matrix, int row, int col, int matrixnum)
    {
        if(matrixnum == 0)
            System.out.println("Printing Matrix");
        else
            System.out.println("Printing Matrix #"+matrixnum);
        for(int rowIndex = 0; rowIndex < row; rowIndex++)
        {
            for(int colIndex = 0; colIndex < col; colIndex++)
            {
                System.out.print(matrix[rowIndex][colIndex]+"\t");
            }
            System.out.println();
        }
    }

    public static void addMatrix(int[][] matrix1, int[][] matrix2, int rows, int cols)
    {
        int[][] resArr = new int[rows][cols];
        int rowIndex, colIndex;
        for(rowIndex = 0; rowIndex < rows; rowIndex++)
        {
            for(colIndex = 0; colIndex < cols; colIndex++)
            {
                resArr[rowIndex][colIndex] = matrix1[rowIndex][colIndex] + matrix2[rowIndex][colIndex];
            }
        }
        System.out.println("Resulting Matrix:");
        printMatrix(resArr, rows, cols,0);
    }

    public static void multiplyMatrix(int[][] matrix1, int[][] matrix2, int rows, int cols)
    {
        int[][] resArr = new int[rows][cols];
        int rowIndex, colIndex;
        for(rowIndex = 0; rowIndex < rows; rowIndex++)
        {
            for(colIndex = 0; colIndex < cols; colIndex++)
            {
                resArr[rowIndex][colIndex] = matrix1[rowIndex][colIndex] * matrix2[rowIndex][colIndex];
            }
        }
        System.out.println("Resulting Matrix:");
        printMatrix(resArr, rows, cols,0);
    }
}
