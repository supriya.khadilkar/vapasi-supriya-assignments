package shoppingProblem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BrandItems implements Serializable
{
    private HashMap<String, ArrayList<String>> accessories;
    private HashMap<String, ArrayList<String>> clothes;

    public HashMap<String, ArrayList<String>> getAccessories()
    {
        return accessories;
    }

    public void setAccessories(HashMap<String, ArrayList<String>> accessories)
    {
        this.accessories = accessories;
    }

    public HashMap<String, ArrayList<String>> getClothes()
    {
        return clothes;
    }

    public void setClothes(HashMap<String, ArrayList<String>> clothes)
    {
        this.clothes = clothes;
    }

    public BrandItems()
    {
        accessories = new HashMap<>();
        clothes = new HashMap<>();
    }
}


/*class Item {
    public Brand brand;
    public Typ type;
    name;
    fabric
}

list<Item>

item.filter(i -> item.brand == brandName && item.type ==type).*/