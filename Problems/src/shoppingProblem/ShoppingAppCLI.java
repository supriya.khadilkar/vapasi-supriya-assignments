package shoppingProblem;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class ShoppingAppCLI
{

    static MyShoppingApp shoppingApp = new MyShoppingApp();

    public static void main(String[] args)
    {
        /*brand1 = ew Brand();
        new Item(name, brand1, "clothing")
        new Item(name, brand1, "clothing")
        new Item(name, brand1, "clothing");
        list*/
        shoppingApp.addBrand("Puma");
        shoppingApp.addBrand("Zara");
        shoppingApp.addBrand("Something");
        //shoppingApp.printAllAccessoryItemsForBrand("Puma");
        ArrayList<String> clothes = new ArrayList<>();
        clothes.add("Blue Jeans"); clothes.add("Red Jeans");
        shoppingApp.addClothingItemToBrand("Puma","Jeans",clothes);
        shoppingApp.addAccessoriesToBrand("Zara", "Belts", new ArrayList<>(Arrays.asList("Blue Belt, Red Belt")));
        Scanner scn = new Scanner(System.in);
        while(true)
        {
            System.out.println("Choose your role:");
            int roleInput = -1, opnInput = -1;
            System.out.println("1.Buyer");
            System.out.println("2.Owner");
            System.out.println("3.Exit");
            roleInput = scn.nextInt();
            switch (roleInput)
            {
                case 1:
                    handleBuyerMenu(scn);
                    break;
                case 2:
                    handleOwnerMenu(scn);
                    break;
                case 3:
                    System.exit(0);
                    break;
            }
        }
    }

    public static void handleBuyerMenu(Scanner scn)
    {
        int opnInput = -1;
        Boolean innerBreak = false;

        String brandName;
        while (true)
        {
            innerBreak = false;
            System.out.println("Choose Operation\n1.Show Brand Products\n2.Search Accessory\n3.Search Clothing\n4.Add Item To Cart\n5.Remove Item from cart\n6.Print Cart\n7.Exit");
            opnInput = scn.nextInt();
            switch (opnInput)
            {
                case 1:
                    System.out.println("Enter Brand Name: ");
                    brandName = scn.next();
                    if(null != brandName && !brandName.isEmpty() && !brandName.isBlank())
                        shoppingApp.printAllProductsForBrand(brandName);
                    break;
                case 2:
                    System.out.println("Enter accessory: ");
                    String accName = scn.next();
                    if(null != accName && !accName.isEmpty() && !accName.isBlank()) {
                        HashMap<String, HashMap<String, ArrayList<String>>> resMap = shoppingApp.searchProductsByAccessory(accName);
                        if (resMap.isEmpty())
                            System.out.println("Currently there are no given accessory");
                        else
                            System.out.println("Brand Name\tAccessory\tItems");
                        resMap.forEach((k, v) -> {
                            System.out.println(k + "\t" + v);
                        });
                    }
                    break;
                case 3:
                    System.out.println("Enter Clothing: ");
                    String clhType = scn.next();
                    if(null != clhType && !clhType.isBlank() && !clhType.isEmpty())
                    {
                        HashMap<String, HashMap<String, ArrayList<String>>> resMap = shoppingApp.searchProductsByClothing(clhType);
                        if (resMap.isEmpty())
                            System.out.println("Currently there are no given clothing type");
                        else
                            System.out.println("Brand Name\tClothing\tItems");
                        resMap.forEach((k, v) -> {
                            System.out.println(k + "\t" + v);
                        });
                    }
                    break;
                case 4:
                    int count;
                    System.out.println("Enter Brand Name: ");
                    brandName = scn.next();
                    System.out.println("Accessory Items:");
                    String pItem = scn.next();
                    System.out.println("Enter Count:");
                    count = scn.nextInt();
                    if(null != brandName && null != pItem && !brandName.isEmpty() && !brandName.isBlank() && !pItem.isBlank() && !pItem.isEmpty())
                        shoppingApp.addItemToCart(brandName, pItem, count);
                    break;
                case 5:
                    System.out.println("Enter Purchased item id:");
                    int cartItemId = scn.nextInt();
                    shoppingApp.removeCartItem(cartItemId);
                    break;
                case 6:
                    System.out.println("Printing Cart");
                    shoppingApp.printMyCart();
                    break;
                case 7:
                    innerBreak = true;
                    break;
            }
            if (innerBreak)
                break;
        }
    }

    public static void handleOwnerMenu(Scanner scn)
    {
        int opnInput = -1;
        Boolean innerBreak1 = false;
        while (true)
        {
            System.out.println("Select Operation:");
            System.out.println("1.Add Brand\n2.Add Accessory with item\n3.Add Clothing with item/items[use comma to separate]\n4.Remove Accessory from Brand\n5.Remove Clothing from brand\n6.Remove Brand\n7.Show Brand Products\n8.Exit");
            opnInput = scn.nextInt();
            switch (opnInput) {
                case 1:
                    String brdName;
                    System.out.println("Add Brand Name: ");
                    brdName = scn.next();
                    shoppingApp.addBrand(brdName);
                    break;
                case 2:
                    String accName, item;
                    System.out.println("Brand Name: ");
                    brdName = scn.next();
                    System.out.println("Accessory Name: ");
                    accName = scn.next();
                    System.out.println("Accessory Items:");
                    item = scn.next();
                    if (item.contains(","))
                    {
                        String[] its = item.trim().split(",");
                        ArrayList<String> items = new ArrayList<>();
                        for (int i = 0; i < its.length; i++) {
                            items.add(its[i]);
                        }
                        shoppingApp.addAccessoriesToBrand(brdName, accName, item);
                    } else
                        shoppingApp.addAccessoriesToBrand(brdName, accName, item);
                    // shoppingApp.printAllAccessoryItemsForBrand(brdName);
                    break;
                case 3:
                    String clhName;
                    System.out.println("Brand Name: ");
                    brdName = scn.next();
                    System.out.println("Clothing Type: ");
                    clhName = scn.next();
                    System.out.println("Clothing Items, please use comma, if providing multiple item in a line");
                    item = scn.next();
                    if (item.contains(","))
                    {
                        String[] its = item.split(",");
                        ArrayList<String> items = new ArrayList<>();
                        for (int i = 0; i < its.length; i++) {
                            items.add(its[i]);
                        }
                        shoppingApp.addClothingItemToBrand(brdName, clhName, items);
                    } else
                        shoppingApp.addAccessoriesToBrand(brdName, clhName, item);
                    break;
                case 4:
                    System.out.println("Brand Name: ");
                    brdName = scn.next();
                    System.out.println("Accessory Name: ");
                    accName = scn.next();
                    if(null != brdName && null != accName && !brdName.isBlank() && !brdName.isEmpty() && !accName.isEmpty() && !accName.isBlank())
                    {
                        shoppingApp.removeAccessoryFromBrand(brdName, accName);
                    }
                    break;
                case 5:
                    System.out.println("Brand Name: ");
                    brdName = scn.next();
                    System.out.println("Clothing Type: ");
                    clhName = scn.next();
                    if(null != brdName && null != clhName && brdName.isBlank() && !brdName.isEmpty() && !clhName.isEmpty() && !clhName.isBlank())
                    {
                        shoppingApp.removeClothingTypeFromBrand(brdName, clhName);
                    }
                    System.out.println("in 5");
                    break;
                case 6:
                    System.out.println("Brand Name: ");
                    brdName = scn.next();
                    if(null != brdName && !brdName.isBlank() && !brdName.isEmpty())
                        shoppingApp.removeBrand(brdName);
                    break;
                case 7:
                    System.out.println("Brand Name: ");
                    brdName = scn.next();
                    if(null != brdName && !brdName.isBlank() && !brdName.isEmpty())
                        shoppingApp.printAllProductsForBrand(brdName);
                    break;
                case 8:
//                    System.out.println("Role Input: " + roleInput);
                    innerBreak1 = true;
                    //                  roleInput = -1;
                    opnInput = -1;
                    break;

            }
            if (innerBreak1)
                break;
        }
    }
}
