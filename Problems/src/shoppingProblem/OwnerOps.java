package shoppingProblem;

import shoppingproblemv1.Brand;
import shoppingproblemv1.Item;
import shoppingproblemv1.ItemType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class OwnerOps
{
/*
    addAccesories,
    addBrand
    removeBrand,
    searchBrand,
    searchAccessoryBrandwise
    removeAccesories



 */
    static volatile List<Item> items = new ArrayList<>();
    static volatile List<Brand> brands = new ArrayList<>();
    static volatile List<ItemType> itemTypeList = new ArrayList<>();

    static void fillInventory()
    {
        Random random = new Random();

        Brand b1 = new Brand("PUMA");
        Brand b2 = new Brand("ZARA");
        Brand b3 = new Brand("NIKE");
        Brand b4 = new Brand("GUCCI");
        Brand b5 = new Brand("PARKER");

        brands.add(b1);
        brands.add(b2);
        brands.add(b3);
        brands.add(b4);
        brands.add(b5);

        ItemType acc = ItemType.Accessory;
        ItemType clh = ItemType.Clothing;
        ItemType lgg = ItemType.Luggage;
        ItemType per = ItemType.Perfume;
        //ItemType

        itemTypeList.add(acc);
        itemTypeList.add(clh);
        itemTypeList.add(lgg);
        itemTypeList.add(per);

        Item it1 = new Item("Red Jeans", random.nextInt(), b1, acc);
        Item it2 = new Item("Purple Bag", random.nextInt(), b4, lgg);
        Item it3 = new Item("Blue Shirt", random.nextInt(), b3, clh);
        Item it4 = new Item("Red Shoes", random.nextInt(), b3, acc);
        Item it5 = new Item("Tanks", random.nextInt(), b2, clh);
        Item it6 = new Item("Capris", random.nextInt(), b2, clh);
        Item it7 = new Item("Vintage Jeans", random.nextInt(), b1, acc);
        Item it8 = new Item("Red Jeans", random.nextInt(), b2, clh);
        Item it9 = new Item("Green Sport Shoes", random.nextInt(), b3, acc);
        Item it10 = new Item("Floaters", random.nextInt(), b3, acc);
        Item it11 = new Item("Duffle Bag", random.nextInt(), b3, acc);
        Item it12 = new Item("Some scent", random.nextInt(), b5, per);
        Item it13 = new Item("Wrist Band", random.nextInt(), b1, acc);
        Item it14 = new Item("Red Skirt", random.nextInt(), b2, clh);
        Item it15 = new Item("Yellow Skirt", random.nextInt(), b2, clh);
        Item it16 = new Item("Tunics", random.nextInt(), b2, clh);
        Item it17 = new Item("Purple Socks", random.nextInt(), b1, acc);
        Item it18 = new Item("Black Eyeglass", random.nextInt(), b5, acc);
        Item it19 = new Item("Red Socks", random.nextInt(), b5, acc);
        Item it20 = new Item("Red Pocket Bag", random.nextInt(), b4, lgg);
        Item it21 = new Item("Yellow Pocket Bag", random.nextInt(), b4, lgg);
        Item it22 = new Item("Laptop Bag", random.nextInt(), b4, lgg);

        items.add(it1);items.add(it2);items.add(it3);
        items.add(it4);items.add(it5);items.add(it6);
        items.add(it7);items.add(it8);items.add(it9);
        items.add(it10);items.add(it11);items.add(it12);
        items.add(it13);items.add(it14);items.add(it15);
        items.add(it16);items.add(it17);items.add(it18);
        items.add(it19);items.add(it20);items.add(it21);
        items.add(it22);
    }
    public void addItems(final Item item)
    {
        boolean noneFound = items.stream().noneMatch(e -> e.equals(item));
        if(noneFound)
            items.add(item);
        else
            System.out.println("Brand is already added");
    }

    public void addItems(final ArrayList<Item> newItems)
    {
        items.addAll(newItems);
    }

    public void addBrand(final String brdName)
    {
        boolean noneFound = brands.stream().noneMatch(e -> e.getBrandName().equals(brdName));
        if(noneFound)
            brands.add(new Brand(brdName));
        else
            System.out.println("Brand is already added");
    }

    public Brand searchBrand(String brdName)
    {

        Optional<Brand> optBrand = brands.stream().filter(b -> b.equals(brdName)).findFirst();
       // if(optBrand.isPresent())
        return optBrand.isPresent() ? optBrand.get() : new Brand();
    }

    public Item searchItemByName(String itemName)
    {
        Optional<Item> optItem = items.stream().filter( it -> it.getItemName().equals(itemName)).findFirst();
        return optItem.isPresent() ? optItem.get() : new Item();
    }


}



