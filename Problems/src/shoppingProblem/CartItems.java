package shoppingProblem;

import java.io.Serializable;

public class CartItems implements Serializable
{
    private String brandName;
    private String purchasedItemName;
    private Integer count;

    public CartItems()
    {
    }

    public CartItems(String brandName, String itemName, Integer count)
    {
        this.brandName = brandName;
        this.purchasedItemName = itemName;
        this.count = count;
    }
    public String getBrandName()
    {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getPurchasedItemName() {
        return purchasedItemName;
    }

    public void setPurchasedItemName(String purchasedItemName) {
        this.purchasedItemName = purchasedItemName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString()
    {
        return "Brand Name:" + this.brandName +"\tPurchased Item:" + this.purchasedItemName +"\tCount: "+ this.count;
    }
}
