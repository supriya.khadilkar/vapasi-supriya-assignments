package shoppingProblem;

import java.util.*;

public class MyShoppingApp
{

     private HashMap<String, BrandItems> inventory;

     HashMap<Integer, CartItems> boughtItems = new HashMap<>();


     public HashMap<String, BrandItems> getInventory() {
          return inventory;
     }

     public void setInventory(HashMap<String, BrandItems> inventory) {
          this.inventory = inventory;
     }

     public MyShoppingApp()
     {
          inventory = new HashMap<>();
     }
     /* only adds brand */
     public void addBrand(String brandName)
     {
          if(!inventory.containsKey(brandName))
          {
               BrandItems br = new BrandItems();
               inventory.put(brandName, br);
          }

     }

     public void removeBrand(String brandName)
     {
          /*
               TODO: throw an exception if accessory-items  or clothing-items are not empty
           */
          if(inventory.containsKey(brandName))
               inventory.remove(brandName);
     }

     /**
      *
      * @param brandName
      * @param accessoryName
      * @param items: ArrayList<String>  passing  variations of accessory items list
      *
      */
     public void addAccessoriesToBrand(String brandName, String accessoryName, ArrayList<String> items)
     {
          if(inventory.containsKey(brandName))
          {
               HashMap<String,ArrayList<String>> currentAccessories = inventory.get(brandName).getAccessories();
               if(null != currentAccessories)
               {
                    if(currentAccessories.containsKey(accessoryName))
                    {
                         if(null != currentAccessories.get(accessoryName))
                           currentAccessories.get(accessoryName).addAll(items);
                    }
                    else
                    {
                         currentAccessories.put(accessoryName, items);
                    }
               }
               else
               {
                    HashMap<String, ArrayList<String>> accessoryMap = new HashMap<>();
                    accessoryMap.put(accessoryName, items);
                    inventory.get(brandName).setAccessories(accessoryMap);
               }
          }
     }
     /**
      *
      * @param brandName
      * @param accessoryName
      * @param item: adds an item to accessory items list
      *             e.g if 'Eyeglass' -> [Red Eyeglass, Frameless Eyeglass]
      *            item = "Purple Eyeglass"
      *           'Eyeglass' -> [Red Eyeglass, Frameless Eyeglass, Purple Eyeglass]
      *       To add an item to existing accessory items list
      */
     public void addAccessoriesToBrand(String brandName, String accessoryName, String item)
     {
          if(inventory.containsKey(brandName))
          {
               HashMap<String,ArrayList<String>> currentAccessories = inventory.get(brandName).getAccessories();
               if(null != currentAccessories)
               {
                    if (currentAccessories.containsKey(accessoryName))
                    {
                         currentAccessories.get(accessoryName).add(item);
                    } else {
                         ArrayList<String> items = new ArrayList<>();
                         items.add(item);
                         currentAccessories.put(accessoryName, items);
                    }
                    inventory.get(brandName).setAccessories(currentAccessories);
               }
               else
               {
                    /*If  accessory items maps is null, then create a map assign that map to it*/
                    ArrayList<String> items =  new ArrayList<>();
                    items.add(item);
                    HashMap<String, ArrayList<String>> latestAccMaps = new HashMap<>();
                    latestAccMaps.put(accessoryName, items);
                    inventory.get(brandName).setAccessories(latestAccMaps);
               }
          }

     }

     public void removeAccessoryFromBrand(String brandName, String accessoryName)
     {
          if(inventory.containsKey(brandName))
          {
               HashMap<String, ArrayList<String>> currentAcMap = inventory.get(brandName).getAccessories();
               if(currentAcMap.containsKey(accessoryName))
               {
                    /*
                         TODO: should throw an exception if accessory item list is not empty
                    *   */
                    currentAcMap.remove(accessoryName);
               }
          }
     }

     public void removeClothingTypeFromBrand(String brandName, String clothingType)
     {
          if(inventory.containsKey(brandName))
          {
               HashMap<String, ArrayList<String>> currentAcMap = inventory.get(brandName).getClothes();
               if(currentAcMap.containsKey(clothingType))
               {
                    /*
                         TODO: should throw an exception if accessory item list is not empty
                    *   */
                    currentAcMap.remove(clothingType);
               }
          }
     }

     public void addClothingItemToBrand(String brandName, String clothingItemName, ArrayList<String> items)
     {
         if(inventory.containsKey(brandName))
         {
              if(null != inventory.get(brandName).getClothes())
              {
                    if(inventory.get(brandName).getClothes().containsKey(clothingItemName))
                    {
                         inventory.get(brandName).getClothes().get(clothingItemName).addAll(items);
                    }
                    else
                    {
                          inventory.get(brandName).getClothes().put(clothingItemName, items);
                    }
              }
         }
     }

     public void  addClothingItemToBrand(String brandName, String clothingItemName, String item)
     {
          if(inventory.containsKey(brandName))
          {
               HashMap<String,ArrayList<String>> currentClothing = inventory.get(brandName).getClothes();
               if(null != currentClothing)
               {
                    if (currentClothing.containsKey(clothingItemName))
                    {
                         currentClothing.get(clothingItemName).add(item);
                    } else {
                         ArrayList<String> items = new ArrayList<>();
                         items.add(item);
                         currentClothing.put(clothingItemName, items);
                    }
                    inventory.get(brandName).setAccessories(currentClothing);
               }
               else
               {
                    /*If  accessory items maps is null, then create a map assign that map to it*/
                    ArrayList<String> items =  new ArrayList<>();
                    items.add(item);
                    HashMap<String, ArrayList<String>> clothMaps = new HashMap<>();
                    clothMaps.put(clothingItemName, items);
                    inventory.get(brandName).setClothes(clothMaps);

               }
          }

     }
     public void printAllBrands()
     {
          if(null != inventory) {
               for (Map.Entry e : inventory.entrySet()) {

                    System.out.println(e.getKey());
               }
          }
     }

     public void printAllAccessoryItemsForBrand(String brandName)
     {
          if(null != inventory)
          {
               HashMap<String, ArrayList<String>> accessories =  inventory.get(brandName).getAccessories();
               System.out.println("For Brand: "+ brandName);
               System.out.println(accessories);
          }
     }

     public void printAllProductsForBrand(String brandName)
     {
          if(null != inventory)
          {
               if(inventory.containsKey(brandName))
               {
                    HashMap<String, ArrayList<String>> accessories = inventory.get(brandName).getAccessories();
                    System.out.println("For Brand: " + brandName + " accessories are:");
                    System.out.println(accessories);

                    HashMap<String, ArrayList<String>> clothing = inventory.get(brandName).getClothes();
                    System.out.println("For Brand: " + brandName + "clothing types are:");
                    System.out.println(clothing);
               }
               else
                    System.out.println("Brand Name: " + brandName + " not found.");
          }

     }

     public ArrayList<String> searchItemsWithBrandAndAccessory(String brandName, String accName)
     {
          ArrayList<String> items = new ArrayList<>();
          if(inventory.containsKey(brandName))
          {
               if(null != inventory.get(brandName).getAccessories())
               {
                    if(inventory.get(brandName).getAccessories().containsKey(accName))
                    {
                         items = inventory.get(brandName).getAccessories().get(accName);
                    }

               }
          }
          return items;
     }

     public HashMap<String, HashMap<String, ArrayList<String>>> searchProductsByAccessory(String accName)
     {
          HashMap<String, HashMap<String, ArrayList<String>>> resultantMap = new HashMap<>();
          //HashMap<String, ArrayList<String>> resultantAccMap = new HashMap<>();
          inventory.forEach((k,v) ->
          {
               if(null != v.getAccessories())
               {

                    if(v.getAccessories().containsKey(accName))
                    {
                         //resultantAccMap = v.getAccessories();
                         resultantMap.put(k, v.getAccessories());
                         //brdsNames.add(k);
                    }
               }
          });
          return  resultantMap;
     }

     public HashMap<String, HashMap<String, ArrayList<String>>> searchProductsByClothing(String clhName)
     {
          HashMap<String, HashMap<String, ArrayList<String>>> resultantMap = new HashMap<>();
          //HashMap<String, ArrayList<String>> resultantAccMap = new HashMap<>();
          inventory.forEach((k,v) ->
          {
               if(null != v.getAccessories())
               {

                    if(v.getAccessories().containsKey(clhName))
                    {
                         //resultantAccMap = v.getAccessories();
                         resultantMap.put(k, v.getClothes());
                         //brdsNames.add(k);
                    }
               }
          });
          return  resultantMap;
     }

     public ArrayList<String> searchClothingWithBrandAndClothingType(String brandName, String clhName)
     {
          ArrayList<String> clsItems = new ArrayList<>();
          if(inventory.containsKey(brandName))
          {
               if(null != inventory.get(brandName).getClothes())
               {
                    if(inventory.get(brandName).getClothes().containsKey(clhName))
                    {
                         clsItems = inventory.get(brandName).getClothes().get(clhName);
                    }
               }
          }
          return clsItems;
     }

     public ArrayList<String> searchBrandByAccessory(String accName)
     {
          ArrayList<String> brdsNames = new ArrayList<>();
          if(null != inventory)
          {
               inventory.forEach((k,v) ->{
                              if(null != v.getAccessories())
                              {
                                   if(v.getAccessories().containsKey(accName))
                                   {
                                        brdsNames.add(k);
                                   }
                              }
               });
          }
          if(brdsNames.isEmpty())
               brdsNames.add("Currently no brand offering accessory type: " + accName);
          return brdsNames;
     }

     public ArrayList<String> searchBrandByClothing(String clhName)
     {
          ArrayList<String> brdsNames = new ArrayList<>();
          if(null != inventory)
          {

               inventory.forEach((k,v) ->{
                    if(null != v.getAccessories())
                    {
                         if(v.getAccessories().containsKey(clhName))
                         {
                              brdsNames.add(k);
                         }
                    }
               });
          }
          if(brdsNames.isEmpty())
               brdsNames.add("Currently no brand offering clothing type: " + clhName);
          return brdsNames;
     }

     public void addItemToCart(String brandName, String itemName, Integer count)
     {

          CartItems cts = new CartItems(brandName, itemName, count);
          if(null == boughtItems)
               boughtItems = new HashMap<>();
          boughtItems.put(getRandomID(), cts);
     }



     public void removeCartItem(Integer purchaseId)
     {
          if(null != boughtItems) {

               if(boughtItems.containsKey(purchaseId)) {
                    boughtItems.remove(purchaseId);
               }
          }
     }

     public void printMyCart()
     {
          if(null != boughtItems)
          {
              boughtItems.forEach((k,v) -> {
                   System.out.println("Id: " + k + v.toString());
              });
          }
     }

     public static Integer getRandomID()
     {
          Random random = new Random(1) ;
          return random.nextInt();
     }
}
