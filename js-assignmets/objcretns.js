var books = {};

function createMyBook(title, price, isbn, author)
{
  this.title = title;
  this.price = price;
  this.isbn = isbn;
  this.author = author;
}

function addtoMyCollection(book)
{
  books.push(book);
}
