var numbers = [1,4,6];

var square = function(n) { return n * n}

const cube = function(n) {return n * n * n}

function sumOfSqrs(nums, sqr)
{
  var sum = 0;
  for(var num of nums)
    {
      sum =  sum + square(num);
    }
  return sum;
}

function sumOfCubes(nums, cubes)
{
  var csum = 0;
  for(var num of nums)
  {
    csum = csum + cube(num);
  }
  return csum;
}

console.log("----", sumOfSqrs(numbers));
console.log("----", sumOfCubes(numbers));
