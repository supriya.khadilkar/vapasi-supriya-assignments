/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Cmd-R),
 * 2. Inspect to bring up an Object Inspector on the result (Cmd-I), or,
 * 3. Display to insert the result in a comment after the selection. (Cmd-L)
 */

function createBook(title, price, author)
{
  //var book = {}
  this.tile = title;
  this["price"] = price;
  this.author = author;
  
  console.log(this);
  //return book;
}

var book1 = new createBook("Some book", 300, "someone");
var newProp = "isbn";
book1[newProp] = "21-2888-33";
console.log(book1);
