var books = [];

function createBook(title, price, author, isbn)
{
  this.title = title;
  this.price = price;
  this.author = author;
  this.isbn = isbn;
}

function addtoMyCollection(book)
{
  books.push(book);
}

var b1 = new createBook("ABC", 600, "asdwea", "123-22-323-0");
var b2 = new createBook("CBBS", 125, "avffva", "123-22-323-0");
var b3 = new createBook("UP and Getting JS", 378, "aewra", "123-22-323-0");
var b4 = new createBook("DS", 600, "reraa", "123-22-323-0");
var b5 = new createBook("Network", 400, "aa", "123-22-323-0");
var b6 = new createBook("DBA", 650, "affa", "123-22-323-0");
var b6 = new createBook("DBA", 285, "eretaa", "123-22-323-0");


function bookspriceGreaterThanTwoHundread()
{
   var rbooks = books.filter((b) => b.price > 200);
   return rbooks;
}

addtoMyCollection(b1);
addtoMyCollection(b2);
addtoMyCollection(b3);
addtoMyCollection(b4);
addtoMyCollection(b5);
addtoMyCollection(b6);

console.log(bookspriceGreaterThanTwoHundread());
